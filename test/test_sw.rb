require 'numru/hdfeos5'
include NumRu


############################################################################### OK
##### MLS データを読み込んで作業ができるようにする
############################################################################### OK
filename1 = "MLS-Aura_L2GP-O3_v02-21-c01_2007d059.he5"
print "reading ",filename1,"...\n"
file1=HE5.open(filename1,"r")
p file1.has_swath?()
p file1.has_grid?()
p file1.has_za?()
p file1.has_point?()

swathlist=file1.swath_names()
p swathlist
p swathlist[0]
########################################
##### Swath1をよむ
########################################
swath1=file1.swath(swathlist[0])
#-------
print "inqdims : \n"
swath1.inqdims('HE5_HDFE_NENTDIM')
#-------
print "inqgeofields : \n"
print "geo_names = " ,swath1.geo_names ,"\n"
print "ndims() = " ,swath1.ndims() ,"\n"
print "dim_names = " ,swath1.dim_names ,"\n"
#-------
print "inqdatafields : \n"
print "var_names = " ,swath1.var_names ,"\n"
swath1.inqdatafields('HE5_HDFE_NENTDFLD')
#-------
print "inqattrs : \n"
swath1.inqattrs()
print "nvars() = " ,swath1.nvars() ,"\n"
print "var_names = " ,swath1.var_names ,"\n"
print "vars() = " ,swath1.vars() ,"\n"
print "vars([]) = " ,swath1.vars(["L2gpValue","L2gpPrecision"]) ,"\n"
print "geos() = " ,swath1.geos() ,"\n"
print "geos([]) = " ,swath1.geos(["Latitude"]) ,"\n"
gname1=swath1.geo("Time")
dname1=swath1.var("L2gpValue")
#-------
print "fieldinfo : \n"
p dname1.fieldinfo()
swath1.detach
file1.close
############################################################################### OK
##### SwathFieldをよむ
########################################
dname1=HE5SwField.open(filename1, swathlist[0],"Latitude")
print "HE5SwField fieldinfo : \n"
p dname1.fieldinfo()
start= [0,0]
edge = [20,10]
p dname1.get_vars_float(start,nil,edge)
#-------
print "inqlocattrs : \n"
p dname1.inqlocattrs()
p dname1.get_att("Units")
p dname1.get_att("Title")
p dname1.get_att("_FillValue")
p dname1.get_att("Units")
p dname1.get_att("MissingValue")
p dname1.get_att("UniqueFieldDefinition")
p dname1.swath.inqdatatype(dname1.name,"Units","HE5_HDFE_GEOGROUP")
dname1.swath.detach
dname1.swath.file.close
############################################################################### OK
dname1=HE5SwField.open(filename1, swathlist[0],"L2gpValue")
print "HE5SwField fieldinfo : \n"
#p dname1.fieldinfo()

start= [0,0]
edge = [20,10]

p dname1.name

dname1.swath.detach
dname1.swath.file.close
############################################################################################
# HDF-EOS5 sample program ( for C )
############################################################################### OK
##### he5_sw_setup.c
############################################################################### OK
filename1 = "test_sw.he5"
print "creating ",filename1,"...\n"
file1=HE5.open(filename1,"w")
swath1=file1.swcreate("Swath1")

swath1.defdim("GeoTrack", 20)
swath1.defdim("GeoXtrack", 10)
swath1.defdim("Res2tr", 40)
swath1.defdim("Res2xtr", 20)
swath1.defdim("Bands",15)
swath1.defdim("IndxTrack", 12)
swath1.defdim("ProfDim", 4)
swath1.defdim("ExtDim", 60)
swath1.defdim("Unlim", 4)
swath1.defdimmap("GeoTrack", "Res2tr", 0, 2)
swath1.defdimmap("GeoXtrack", "Res2xtr", 1, 2)
ary=[0,1,3,6,7,8,11,12,14,24,32,39]
swath1.defidxmap("IndxTrack", "Res2tr",ary)
swath1.detach
file1.close
############################################################################### ok
##### he5_sw_defunlimfld.c he5_sw_definefields.c
############################################################################### OK
print "add1 ",filename1,"...\n"
file2=HE5.open(filename1,"a")
swath2=file2.swath("Swath1")
swath2.def_geo("Time", "float", "GeoTrack")
swath2.def_geo("Longitude", "sfloat", "GeoTrack,GeoXtrack")
swath2.def_geo("Latitude", "sfloat", "GeoTrack,GeoXtrack")
fillvalue = "charname"
p swath2.setfillvalue("Test_string","char",fillvalue)
swath2.def_var("Test_string", "char", "GeoXtrack")
swath2.def_var("Density", "sfloat", "GeoTrack")
fillvalue2 = -777.0
p swath2.setfillvalue("Temperature","sfloat",fillvalue2)
swath2.def_var("Temperature", "sfloat", "GeoTrack,GeoXtrack")
swath2.def_var("Pressure", "float", "Res2tr,Res2xtr")

RANK = 3
chunk_dims=[15,40,20]
p swath2.defchunk(RANK,chunk_dims)
comp_code = "HE5_HDFE_COMP_NONE"
comp_level=[8,0,0,0,0]
p swath2.defcomp(comp_code,comp_level)
swath2.defdatafield("Spectra", "Bands,Res2tr,Res2xtr",  "NULL", "sfloat", 0)
swath2.defdatafield("Count", "GeoTrack", "NULL", "int", 0);

swath2.prdefine("Profile-2000", "ProfDim", "NULL", "int")
swath2.detach
file2.close
############################################################################### ok
##### he5_sw_writedata.c 
############################################################################### OK
print "add3 ",filename1,"...\n"
file3=HE5.open(filename1,"a")
swath3=file3.swath("Swath1")

start= [0,0]
#edge = [9,19]
edge = [-1,-1]

tmp = NArray.float(1,20)
lat = tmp * NArray.float(10,1) 
lng = tmp * NArray.float(10,1)

lat[] = 1.0
lng[] = 5.0
dname1=swath3.var("Longitude")
dname1.writefield(start,nil, edge, lng,"sfloat")
print "dim_names check"
p dname1.dim_names
dname1=swath3.var("Latitude")
dname1.writefield(start,nil, edge, lat,"sfloat")

ttime = NArray.float(20)
for i in 0..19
  ttime[i] =  5.0e7 + 5.0e6 * i 
end

tstart = [0]
tedge = [19]
dname1=swath3.var("Time")
dname1.writefield(tstart,nil, tedge, ttime,"float")
#tstart = [0]
#tedge = [9]
#charname=["A","A","A","A","A","A","A","A","A","A"]
#dname1=swath3.var("Test_string")
#dname1.writefield(tstart, nil, tedge, charname,"H5T_NATIVE_CHAR")

start= [0,0,0]
edge = [14,39,19]
tmp = NArray.float(1,40,20)
spectra = tmp * NArray.float(15,1)
spectra[] = 20.0000
p "spectra =",spectra
dname1=swath3.var("Spectra")
dname1.writefield(start,nil, edge, spectra,"sfloat")
############################################################################### OK
edge[0]=4
attr1 = [1, 2, 3, 4]
swath3.writeattr("GlobalAttribute","int",edge,attr1)

edge[0]=8
attr1 = "ABCDEFGH" 
swath3.writeattr("GLOBAL_CHAR_ATTR","char",edge,attr1)
edge[0]=3
attr1 = "abc"
swath3.writeattr("GLOBAL_CHAR_ATTR_11","char",edge,attr1)
# Long 型への対応
#edge[0]=4
#attr1 = [1111111,2222222,3333333,4444444]
#swath3.writeattr("GLOBAL_LONG_ATTR","H5T_NATIVE_LONG",edge,attr1)
edge[0]=4
attr1 = [1.111111,2.222222,3.333333,4.444444]
swath3.writeattr("GLOBAL_DOUBLE_ATTR","float",edge,attr1)
edge[0]=4
attr1 = [10, 20, 30, 40]
swath3.writegrpattr("GroupAttribute","int",edge,attr1)
edge[0]=18
attr1 = "ababababababababab"
dname1=swath3.var("Time")
dname1.writelocattr("LOCAL_CHAR_ATTR","char",edge,attr1)
#edge[0]=4
#attr1 = [100, 200, 300, 400]
#dname1=swath3.var("Density")
#dname1.writelocattr("LocalAttribute_1","H5T_NATIVE_INT",edge,attr1)
#edge[0]=4
#attr1 = [1.111111,2.222222,3.333333,4.444444]
#dname1=swath3.var("Longitude")
#dname1.writelocattr("LocalAttribute_2","H5T_NATIVE_FLOAT",edge,attr1)
swath3.detach
file3.close
############################################################################### OK
##### he5_sw_wrexternaldata.c
############################################################################### OK
print "add4 ",filename1,"...\n"
file4=HE5.open(filename1,"a")
swath4=file4.swath("Swath1")

size = [10, 20, 30]
offset = [10, 40, 80]
swath4.defdatafield("ExtData", "ExtDim", "NULL", "int", 0)
swath4.detach
file4.close
############################################################################### OK
##### he5_sw_info.c
############################################################################### OK
print "info1 ",filename1,"...\n"
file4=HE5.open(filename1,"r")
swath4=file4.swath("Swath1")
p swath4.inqdims('HE5_HDFE_NENTDIM')
p swath4.inqmaps('HE5_HDFE_NENTMAP')
p swath4.inqidxmaps('HE5_HDFE_NENTIMAP')
p swath4.inqgeofields('HE5_HDFE_NENTGFLD')
p swath4.inqdatafields('HE5_HDFE_NENTDFLD')
p swath4.diminfo('GeoTrack')
p swath4.mapinfo('GeoTrack',"Res2tr")
p swath4.diminfo('IndxTrack')
dname1=swath4.var("Longitude")
p dname1.fieldinfo()
dname1=swath4.var("Test_string")
p dname1.fieldinfo()
p swath4.inqattrs()
dname1=swath4.var("Spectra")
p dname1.inqlocattrs()
p dname1.compinfo()
p swath4.chunkinfo("Spectra")
swath4.detach
file4.close
############################################################################### OK
##### he5_gd_readdata.c
############################################################################### OK
print "info3 ",filename1,"...\n"
file6=HE5.open(filename1,"r")
swath6=file6.swath("Swath1")
dname1=swath6.var("Longitude")

start= [0,0]
edge = [19,9]
print "readfield \n"
p dname1.readfield(start, nil, edge,"sfloat")
p swath6.get_att_("GlobalAttribute")
p swath6.get_grpatt_("GroupAttribute")
swath6.detach
file6.close
############################################################################### ok
