require 'numru/hdfeos5'
include NumRu


############################################################################### OK
##### OMI データを読み込んで作業ができるようにする
############################################################################### OK
filename1 = "OMI-Aura_L3-OMAEROe_2008m0101_v003-2009m0114t114202.he5"
print "reading ",filename1,"...\n"
file1=HE5.open(filename1,"r")
p file1.has_swath?()
p file1.has_grid?()
p file1.has_za?()
p file1.has_point?()
gridlist=file1.grid_names()
p gridlist[0]
########################################
##### Gridをよむ
########################################
grid1=file1.grid(gridlist[0])
#-------
print "ndims = " ,grid1.ndims ,"\n"
print "inqdims =", grid1.inqdims('HE5_HDFE_NENTDIM'),"\n"

#-------
print "inqdatafields : \n"
print "var_names : " , grid1.var_names ,"\n"
#-------
print "inqattrs : "  ,grid1.inqattrs(), "\n"
print "nvars() = "   ,grid1.nvars() ,"\n"
print "var_names = " ,grid1.var_names ,"\n"
#-------
dname1=grid1.var("AerosolModelMW")
print "fieldinfo : \n"
p dname1.fieldinfo()
grid1.close
############################################################################### OK
##### GridFieldをよむ
########################################
dname1=HE5GdField.open(filename1, gridlist[0],"AerosolModelMW")
p "HE5GdField fieldinfo : \n"
p dname1.fieldinfo()
start= [0,0]
edge = [100,300]
p dname1.readfield([0,0],[1,1],[100,300],"sint")
#-------
print "inqlocattrs : \n"
p dname1.inqlocattrs()
p dname1.get_att("Units")
p dname1.get_att("Title")
p dname1.get_att("_FillValue")
p dname1.get_att("MissingValue")
p dname1.get_att("UniqueFieldDefinition")
p dname1.grid.inqdatatype(dname1.name,"Title","HE5_HDFE_DATAGROUP")
dname1.grid.close
############################################################################### OK
dname1=HE5GdField.open(filename1, gridlist[0],"Longitude")
print "HE5GdField fieldinfo : \n"
p dname1.fieldinfo()

start= [0,0]
edge = [20,10]

p dname1.readfield([0,0],[1,1],[20,10],"sfloat")
p dname1.name
p dname1.get({"start"=>[3,5]})


dname1.grid.close
############################################################################################
# HDF-EOS5 sample program ( for C )
############################################################################### OK
##### he5_gd_setup.c
############################################################################### OK
filename1 = "test_gd.he5"
print "creating ",filename1,"...\n"
file1=HE5.open(filename1,"w")

uplft = [210584.50041, 322395.95445]
lowrgt = [813931.10959, 2214162.53278]
xdim=120
ydim=200
grid1=file1.gdcreate("UTMGrid",xdim,ydim,uplft,lowrgt)
zonecode=40
spherecode=0
projparm = NArray.float(16)
projparm[] = 0
grid1.defproj("HE5_GCTP_UTM", zonecode, spherecode, projparm)

grid1.defdim("Time", 10)
grid1.defdim("ExtDim", 60)
grid1.defdim("XDim", 120)
grid1.defdim("YDim", 200)

file1.close
file2=HE5.open(filename1,"a")

spherecode = 3
projparm[4] = 0.0
projparm[5] = 90000000.00
xdim=100
ydim=100
grid2=file2.gdcreate("PolarGrid",xdim,ydim,uplft,lowrgt)
grid2.defproj("HE5_GCTP_PS", zonecode, spherecode, projparm)

grid2.deforigin("HE5_HDFE_GD_LR")
grid2.defdim("Bands", 3)
file2.close
############################################################################### OK
##### he5_gd_defunlimfld.c he5_sw_definefields.c
############################################################################### OK
print "add2 ",filename1,"...\n"
file2=HE5.open(filename1,"a")

tilerank = 2
tiledims = [ 100, 60, 0, 0, 0, 0, 0, 0 ]
compparm = [ 8, 0, 0, 0, 0]
grid2=file2.grid("UTMGrid")
grid2.def_var("Pollution", "sfloat", "Time,YDim,XDim")
fillvalue2 = -7.0
p grid2.setfillvalue("Pollution","sfloat",fillvalue2)
grid2.deftile("HE5_HDFE_TILE", tilerank, tiledims)
grid2.defcomp("HE5_HDFE_COMP_DEFLATE", compparm)
grid2.def_var("Vegetation", "sfloat", "YDim,XDim")
grid2.detach()

grid2=file2.grid("PolarGrid")
grid2.def_var("Temperature", "sfloat", "YDim,XDim")
grid2.def_var("Pressure", "sfloat", "YDim,XDim")
grid2.def_var("Soil Dryness", "sfloat", "YDim,XDim")
grid2.def_var("Spectra", "float", "Bands,YDim,XDim")
fillvalue2 = -9999.0
p grid2.setfillvalue("Pressure","sfloat",fillvalue2)
grid2.detach()
############################################################################### OK
##### he5_gd_writedata.c 
############################################################################### OK
print "add3 ",filename1,"...\n"
file3=HE5.open(filename1,"a")
grid3=file3.grid("UTMGrid")
dname1=grid3.var("Vegetation")

start= [0,0]
edge = [200,120]
tmp = NArray.float(1,121)
veg = tmp * NArray.float(201,1)
veg[] = 1.0
dname1.writefield(start, nil, edge, veg,"sfloat")
edge[0] = 1
flt = 3.1415
grid3.writeattr("GlobalAttribute","sfloat",edge,flt)
edge[0] = 3
grpattr = [3,7,11]
grid3.writegrpattr("GroupAttribute","int",edge,grpattr)
edge[0] = 4
attr = [1.1,2.2,3.3,4.4]
dname1.writelocattr("LocalAttribute","sfloat",edge,attr)
grid2.detach()

grid3=file3.grid("PolarGrid")
dname1=grid3.var("Temperature")

start= [0,0]
edge = [100,100]
tmp = NArray.float(1,101)
temp = tmp * NArray.float(101,1)
temp[] = 100.0
dname1.writefield(start, nil, edge, temp,"sfloat")
grid3.close
############################################################################### OK
##### he5_gd_info.c
############################################################################### OK
print "info1 ",filename1,"...\n"
file4=HE5.open(filename1,"r")
grid41=file4.grid("UTMGrid")
grid42=file4.grid("PolarGrid")

p grid41.inqdims('HE5_HDFE_NENTDIM')
p grid42.inqdims('HE5_HDFE_NENTDIM')
p grid41.diminfo('Time')
p grid42.diminfo('Bands')
p grid41.gridinfo()
p grid42.gridinfo()
p grid41.projinfo()
p grid42.projinfo()
p grid41.inqfields("HE5_HDFE_NENTDIM")
p grid42.inqfields("HE5_HDFE_NENTDIM")
p grid41.inqfields("HE5_HDFE_NENTDFLD")
p grid42.inqfields("HE5_HDFE_NENTDFLD")
############################################################################### OK
dname2=grid42.var("Spectra")
p dname2.fieldinfo()
dname1=grid41.var("Vegetation")
p dname1.fieldinfo()

#p grid41.attrinfo()
#p grid41.grpattrinfo()
#p dname1.locattrinfo()
p grid41.inqattrs()
#p grid41.inqgrpattrs()
p dname1.inqlocattrs()
p dname1.tileinfo()
p dname1.compinfo()
############################################################################### ?
##### he5_gd_datainfo.c
############################################################################### ?
print "info2 ",filename1,"...\n"
file5=HE5.open(filename1,"r")
grid51=file5.grid("UTMGrid")
grid52=file5.grid("PolarGrid")

### datatype が文字化けして出てしまう！
p grid51.inqdatatype("Vegetation","NULL",'HE5_HDFE_DATAGROUP')
p grid51.inqdatatype("NULL","GlobalAttribute",'HE5_HDFE_ATTRGROUP')
p grid51.inqdatatype("NULL","GroupAttribute",'HE5_HDFE_GRPATTRGROUP')
p grid51.inqdatatype("Vegetation","LocalAttribute",'HE5_HDFE_LOCATTRGROUP')
file5.close
############################################################################### OK
##### he5_gd_readdata.c
############################################################################### OK
print "info3 ",filename1,"...\n"
file6=HE5.open(filename1,"r")
grid61=file6.grid("UTMGrid")
grid62=file6.grid("PolarGrid")
dname1=grid61.var("Vegetation")

start= [0,0]
edge = [200,120]
p dname1.readfield(start, nil, edge,"sfloat")
p grid61.get_att_("GlobalAttribute")
p grid61.get_grpatt_("GroupAttribute")
p dname1.get_att_("LocalAttribute")

file6.close
###############################################################################
##### he5_gd_subset.c    (途中まで)
###############################################################################
#print "add1 ",filename1,"...\n"
#file2=HE5.open(filename1,"a")
#grid2=file2.grid("PolarGrid")
#cornerlon = NArray.float(2) 
#cornerlat = NArray.float(2) 
#cornerlon[0] = 0.0
#cornerlon[1] = 90.0
#cornerlat[1] = 0.0
#cornerlat[0] = 90.0
#regionid=grid3.defboxregion(cornerlon, cornerlat)
#exit
#regionid.regioninfo("Temperature", "int")
#grid2.detach()
#file2.close()
############################################################################### OK
exit
