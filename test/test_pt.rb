require 'numru/hdfeos5'
include NumRu


############################################################################### OK
##### MLS データを読み込んで作業ができるようにする
############################################################################################
# HDF-EOS5 sample program ( HDFEOS5_ug_vol1_may_2004.pdf )
############################################################################### OK
# sample 1
############################################################################### OK
filename1 = "test_pt.he5"
print "creating ",filename1,"...\n"
file1=HE5.open(filename1,"w")
point1=file1.ptcreate("Simple point")
point2=file1.ptcreate("FixedBuoy point")
point3=file1.ptcreate("FloatBuoy point")

point1.detach
point2.detach
point3.detach
file1.close
############################################################################### ok
# sample 2
############################################################################### OK
print "add2 ",filename1,"...\n"
file2=HE5.open(filename1,"a")

point4=file2.point("Simple point")
nflds=3
rank=[1,1,1]
fieldlist="Time,Concentration,Species"
dims = NArray.int(4)
dims[0] = [1,0]
dims[1] = [1,0]
dims[2] = [1,0]
dtype="float,sfloat,int"
point4.deflevel("Sensor", nflds, rank, fieldlist, dims, dtype)
point4.detach

point5=file2.point("FixedBuoy point")
nflds=5
rank=[1,1,1,1,1]
fieldlist="Label,Longitude,Latitude,DeployDate,ID"
dims = NArray.int(6)
dims[0] = [1,0]
dims[1] = [1,0]
dims[2] = [1,0]
dims[3] = [1,0]
dims[4] = [1,0]
dtype="int,float,float,int,int"
point5.deflevel("Desc-Loc", nflds, rank, fieldlist, dims, dtype)

nflds=4
rank=[1,1,1,1]
fieldlist="Time,Rainfall,Temperature,ID"
dims = NArray.int(5)
dims[0] = [1,0]
dims[1] = [1,0]
dims[2] = [1,0]
dims[3] = [1,0]
dtype="float,sfloat,sfloat,int"
point5.deflevel("observations", nflds, rank, fieldlist, dims, dtype)
point5.deflinkage("Desc-Loc","observations", "ID")
point5.detach

point6=file2.point("FloatBuoy point")
nflds=4
rank=[1,1,1,1]
fieldlist="Label,DaployDate,Weight,ID"
dims = NArray.int(5)
dims[0] = [1,0]
dims[1] = [1,0]
dims[2] = [1,0]
dims[3] = [1,0]
dtype="int,int,int,int"
point6.deflevel("Description", nflds, rank, fieldlist, dims, dtype)

nflds=6
rank=[1,1,1,1,1,1]
fieldlist="Time,Longitude,Latitude,Rainfall,Temperature,ID"
dims = NArray.int(7)
dims[0] = [1,0]
dims[1] = [1,0]
dims[2] = [1,0]
dims[3] = [1,0]
dims[4] = [1,0]
dims[5] = [1,0]
dtype="float,float,float,sfloat,sfloat,int"
point6.deflevel("Measurements", nflds, rank, fieldlist, dims, dtype)
point6.deflinkage("Description","Measurements", "ID")
point6.detach
file2.close
############################################################################### ok
# sample 3
############################################################################### OK
print "add3 ",filename1,"...\n"
file3=HE5.open(filename1,"a")
point7=file2.point("Simple point")
dname1=point7.var("Time")
count =[15]
time = NArray.float(16)
time[] = 1.5555
dname1.put(time)
dname1=point7.var("Concentration")
count =[15]
tmp = NArray.sfloat(1,16)
conc= tmp * NArray.sfloat(5,1)
conc[] = 0.2
dname1.writelevel(count,conc,"sfloat")
dname1=point7.var("Species")
spc= NArray.int(16)
spc = 10
dname1.put(spc)

point8=file2.point("FixedBuoy point")
dname1=point8.var("Longitude")
count =[5]
time = NArray.float(6)
time= [100,110,120,130,140]
dname1.writelevel(count, time,"float")

dname1=point8.var("Latitude")
count =[3]
#time = NArray.float(6)
time = [10,10,20]
dname1.writelevel(count, time,"float")

dname1=point8.var("DeployDate")
count =[5]
time = NArray.int(6)
time = [140,141,141,141,142]
dname1.writelevel(count, time,"int")

dname1=point8.var("ID","Desc-Loc")
count =[5]
time = NArray.int(6)
time = [1,2,3,4,5]
dname1.writelevel(count, time,"int")

dname1=point8.var("Time")
count =[15]
time = NArray.float(16)
time[] = 0.002
dname1.writelevel(count, time,"float")

dname1=point8.var("Rainfall")
count =[15]
time = NArray.sfloat(16)
time[] = 0.234
dname1.put(time)                                               

dname1=point8.var("Temperature")
count =[15]
time = NArray.sfloat(16)
time[] = 233.43
dname1.put(time)                                               

dname1=point8.var("ID","observations")
count =[15]
time = NArray.int(16)
time = [1,2,3,4,5,1,2,3,4,5,1,2,3,4,5]
dname1.writelevel(count, time,"int")

count = [4]
attr1 = [1, 2, 3, 4]
point8.writeattr("GlobalAttribute","int",count,attr1)

count = [8]
attr1 = "ABCDEFGH" 
point8.writeattr("GLOBAL_CHAR_ATTR","char",count,attr1)
count = [3]
attr1 = "abc"
point8.writeattr("GLOBAL_CHAR_ATTR_11","char",count,attr1)

attr1 = [1.111111,2.222222,3.333333,4.444444]
count = [4]
point8.writeattr("GLOBAL_DOUBLE_ATTR","float",count,attr1)
count = [4]
attr1 = [10, 20, 30, 40]
point8.writegrpattr("GroupAttribute","int",count,attr1)
count = [18]
attr1 = "ababababababababab"
dname1=point8.var("Temperature")
dname1.writelocattr("LOCAL_CHAR_ATTR","char",count,attr1)
point8.detach
file3.close
############################################################################### OK
# sample 4
############################################################################### OK
print "info1 ",filename1,"...\n"
file4=HE5.open(filename1,"a")
p file4.has_swath?()
p file4.has_grid?()
p file4.has_za?()
p file4.has_point?()

point8=file4.point("FixedBuoy point")
p point8.getlevelname()
dname1=point8.var("Temperature")
p dname1.get_vars_float()  # error message is shown but it's normal
dname1=point8.var("Latitude")
p dname1.get_vars_double()
dname1=point8.var("DeployDate")
p dname1.get_vars_int()
p dname1.ntype
p dname1.shape
p dname1.ndims
p dname1.dim_names
p dname1.natts
p dname1.fieldinfo()
p dname1.get
p dname1.fwdlink
#p dname1.bcklink
point8.detach

point8=file4.point("FixedBuoy point")
p point8.get_att_("GlobalAttribute")
p point8.get_grpatt_("GroupAttribute")
p point8.nrecs(1)
p point8.nfields
p point8.nlevels
point8.detach
file4.close

file4=HE5.open(filename1,"r")
point9=file4.point("Simple point")
dname1=point9.var("Time")
p dname1.point
p dname1.ntype
p dname1.shape
p dname1.ndims
p dname1.dim_names
p dname1.natts
p dname1.fieldinfo()
p dname1.get                # error message is shown but it's normal
p dname1.get_vars_double()  # error message is shown but it's normal
point9.detach                         
file4.close

file4=HE5.open(filename1,"r")
point7=file4.point("FixedBuoy point")
dname1=point7.var("Latitude")
p dname1.point
p dname1.ntype
p dname1.shape
p dname1.ndims
p dname1.dim_names
p dname1.natts
p dname1.fieldinfo()
p dname1.get
point7.detach                         
file4.close
############################################################################### OK
# sample 5
############################################################################### OK
print "info2 ",filename1,"...\n"
file5=HE5.open(filename1,"a")
point8=file5.point("FixedBuoy point")
dname1=point8.var("Latitude")
dname1.updatelevel(1,[0],13.0,"float")
point8.detach
file5.close

exit
############################################################################### ok
