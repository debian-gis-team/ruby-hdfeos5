require 'numru/hdfeos5'
include NumRu


############################################################################################
# HDF-EOS5 sample program ( for C )
############################################################################### OK
##### he5_za_setup.c
############################################################################### OK
filename1 = "test_za.he5"
print "creating ",filename1,"...\n"
file1=HE5.open(filename1,"w")
za1=file1.zacreate("za1")

za1.defdim("MyTrack1", 20)
za1.defdim("MyTrack2", 10)
za1.defdim("Res2tr", 40)
za1.defdim("Res2xtr", 20)
za1.defdim("Bands",15)
za1.defdim("IndxTrack", 12)
za1.defdim("ProfDim", 4)
za1.defdim("ExtDim",60)
za1.defdim("Unlim", 4)
za1.detach
file1.close
############################################################################### ok
##### he5_za_defunlimfld.c he5_za_definefields.c
############################################################################### OK
print "add1 ",filename1,"...\n"
file2=HE5.open(filename1,"a")
za2=file2.zonal("za1")
za2.def_var("Density", "sfloat", "MyTrack1")
za2.def_var("Temperature", "sfloat", "MyTrack1,MyTrack2")
za2.def_var("Presure", "float", "Res2tr,Res2xtr")
za2.def_var("Test_string", "text", "MyTrack2")
fillvalue2 = "AAAA"
p za2.setfillvalue("Test_string","text",fillvalue2)

RANK = 3
chunk_dims=[15,40,20]
p za2.defchunk(RANK,chunk_dims)
za2.define("Spectra", "Bands,Res2tr,Res2xtr",  "NULL", "sfloat")
comp_code = "HDFE_COMP_SZIP_K13orEC"
comp_level=[16,0,0,0,0]
#za2.defcomp(comp_code,comp_level)
za2.define("Count", "MyTrack1",  "NULL", "int")

za2.detach
file2.close
############################################################################### ok
##### he5_za_writedata.c 
############################################################################### OK
print "add3 ",filename1,"...\n"
file3=HE5.open(filename1,"a")
za3=file3.zonal("za1")

#tstart = [0]
#tedge = [9]
#charname=["A","A","A","A","A","A","A","A","A","A"]
#dname1=za3.var("Test_string")
#dname1.writefield(tstart, nil, tedge, charname,"HE5T_CHARSTRING")

start= [0,0,0]
edge = [14,39,19]
tmp = NArray.float(1,40,20)
spectra = tmp * NArray.float(15,1)
spectra[] = 20.0000
p "spectra =",spectra
dname1=za3.var("Spectra")
dname1.writefield(start,nil, edge, spectra,"sfloat")
############################################################################### OK
edge[0]=4
attr1 = [1, 2, 3, 4]
za3.writeattr("GlobalAttribute","int",edge,attr1)

edge[0]=8
attr1 = "ABCDEFGH"
za3.writeattr("GLOBAL_CHAR_ATTR","char",edge,attr1)
edge[0]=3
attr1 = "abc"
za3.writeattr("GLOBAL_CHAR_ATTR_11","char",edge,attr1)
# Long �^�ւ̑Ή�
#edge[0]=4
#attr1 = [1111111,2222222,3333333,4444444]
#za3.writeattr("GLOBAL_LONG_ATTR","long",edge,attr1)
edge[0]=4
attr1 = [1.111111,2.222222,3.333333,4.444444]
za3.writeattr("GLOBAL_DOUBLE_ATTR","float",edge,attr1)
edge[0]=4
attr1 = [10, 20, 30, 40]
za3.writegrpattr("GroupAttribute","int",edge,attr1)
edge[0]=4
attr1 = [100, 200, 300, 400]
dname1=za3.var("Density")
dname1.writelocattr("LocalAttribute_1","int",edge,attr1)
za3.detach
file3.close
############################################################################### OK
##### he5_za_wrexternaldata.c
############################################################################### OK
print "add4 ",filename1,"...\n"
file4=HE5.open(filename1,"a")
za4=file4.zonal("za1")

size = [10, 20, 30]
offset = [10, 40, 80]
za4.define("ExtData", "ExtDim", "NULL", "int")
za4.detach
file4.close
############################################################################### OK
##### he5_za_info.c
############################################################################### OK
print "info1 ",filename1,"...\n"
file4=HE5.open(filename1,"r")
za4=file4.zonal("za1")
p za4.inqdims('HE5_HDFE_NENTDIM')
p za4.inquire('HE5_HDFE_NENTDFLD')
p za4.diminfo('MyTrack1')
p za4.inqattrs()
p za4.inqgrpattrs()
dname1=za4.var("Density")
p dname1.inqlocattrs()
p dname1.compinfo()
p dname1.dim_names
p dname1.att_names
p dname1.ntype
p dname1.shape
p za4.chunkinfo("Spectra")
#-------
print "ndims() = " ,za4.ndims() ,"\n"
print "dim_names = " ,za4.dim_names ,"\n"
#-------
print "inqattrs =" , za4.inqattrs() ,"\n"
print "nvars() = " ,za4.nvars() ,"\n"
print "var_names = " ,za4.var_names ,"\n"
print "vars() = " ,za4.vars() ,"\n"
za4.detach
file4.close
############################################################################### OK
##### he5_gd_readdata.c
############################################################################### OK
print "info3 ",filename1,"...\n"
file6=HE5.open(filename1,"r")
za6=file6.zonal("za1")
dname1=za6.var("Spectra")

start= [0,0,0]
edge = [14,39,19]
p dname1.readfield(start, nil, edge,"sfloat")
p za6.get_att_("GlobalAttribute")
p za6.get_grpatt_("GroupAttribute")
dname1=za6.var("Density")
p dname1.get_att_("LocalAttribute_1")

file6.close
############################################################################### ok
