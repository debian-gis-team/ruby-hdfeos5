#include<stdio.h>
#include<hdf5.h>
#include<HE5_HdfEosDef.h>
#include "ruby.h"
#include "narray.h"
#include<string.h>

/* for compatibility with ruby 1.6 */
#ifndef RSTRING_PTR
#define RSTRING_PTR(s) (RSTRING(s)->ptr)
#endif
#ifndef SafeStringValue
#define SafeStringValue(s) Check_SafeStr(s)
#endif

static VALUE rb_eHE5Error;

void
HE5Wrap_make_NArray1D_or_str(int ntype, size_t count, VALUE *obj, void **ptr){

    switch (ntype){
    case  HE5T_NATIVE_CHAR:
    case  HE5T_NATIVE_SCHAR:
    case  HE5T_NATIVE_UCHAR:
    case  HE5T_CHARSTRING:
	*ptr = ALLOCA_N(char,count+1);
	*obj = rb_str_new(*ptr,count);
	*ptr = RSTRING_PTR(*obj);
	break;

    case  HE5T_NATIVE_INT8:
    case  HE5T_NATIVE_UINT8:
	*obj = na_make_object(NA_BYTE, 1, &count, cNArray);
	break;

    case  HE5T_NATIVE_SHORT:
    case  HE5T_NATIVE_USHORT:
    case  HE5T_NATIVE_INT16:
    case  HE5T_NATIVE_UINT16:
	*obj = na_make_object(NA_SINT, 1, &count, cNArray);
	break;

    case  HE5T_NATIVE_INT:
    case  HE5T_NATIVE_UINT:
    case  HE5T_NATIVE_INT32:
    case  HE5T_NATIVE_UINT32:
    case  HE5T_NATIVE_LONG:
	*obj = na_make_object(NA_LINT, 1, &count, cNArray);
	break;

    case  HE5T_NATIVE_FLOAT:
	*obj = na_make_object(NA_SFLOAT, 1, &count, cNArray);
	break;

    case  HE5T_NATIVE_DOUBLE:
	*obj = na_make_object(NA_DFLOAT, 1, &count, cNArray);
	break;
    default:
	rb_raise(rb_eHE5Error, "Sorry, number type %d is yet to be supoorted [%s:%d]",
		 ntype,__FILE__,__LINE__);
    }

    switch (ntype){
    case  HE5T_NATIVE_CHAR:
    case  HE5T_NATIVE_SCHAR:
    case  HE5T_NATIVE_UCHAR:
    case  HE5T_CHARSTRING:
	break;
    default:
	*ptr = (void*)((struct NARRAY*)DATA_PTR(*obj))->ptr;
    }
}

void
HE5Wrap_store_NArray1D_or_str(int ntype, VALUE obj, void **ptr){

    struct NARRAY *na;
    switch (ntype){
    case  HE5T_NATIVE_CHAR:
    case  HE5T_NATIVE_SCHAR:
    case  HE5T_NATIVE_UCHAR:
    case  HE5T_CHARSTRING:
        switch (TYPE(obj)){
        case T_STRING:
          SafeStringValue(obj);
          *ptr = (char *) RSTRING_PTR(obj);
          break;
        case T_ARRAY:
          obj = na_cast_object(obj, NA_BYTE);
          GetNArray(obj, na);
          *ptr = (char *) NA_PTR(na,0);
          break;
        }
	break;
    case  HE5T_NATIVE_INT8:
    case  HE5T_NATIVE_UINT8:
        obj = na_cast_object(obj, NA_BYTE);
        GetNArray(obj, na);
        *ptr = (u_int8_t *) NA_PTR(na,0);
	break;

    case  HE5T_NATIVE_SHORT:
    case  HE5T_NATIVE_USHORT:
    case  HE5T_NATIVE_INT16:
    case  HE5T_NATIVE_UINT16:
        obj = na_cast_object(obj, NA_SINT);
        GetNArray(obj, na);
        *ptr = (int16_t *) NA_PTR(na,0);
	break;

    case  HE5T_NATIVE_INT:
    case  HE5T_NATIVE_UINT:
    case  HE5T_NATIVE_INT32:
    case  HE5T_NATIVE_UINT32:
        obj = na_cast_object(obj, NA_LINT);
        GetNArray(obj, na);
        *ptr = (int32_t *) NA_PTR(na,0);
	break;

    case  HE5T_NATIVE_LONG:
        obj = na_cast_object(obj, NA_LINT);
        GetNArray(obj, na);
        *ptr = (long *) NA_PTR(na,0);
	break;

    case  HE5T_NATIVE_FLOAT:
        obj = na_cast_object(obj, NA_SFLOAT);
        GetNArray(obj, na);
        *ptr = (float *) NA_PTR(na,0); \
	break;

    case  HE5T_NATIVE_DOUBLE:
        obj = na_cast_object(obj, NA_DFLOAT);
        GetNArray(obj, na);
        *ptr = (double *) NA_PTR(na,0); \
	break;
    default:
	rb_raise(rb_eHE5Error, "Sorry, number type %d is yet to be supoorted [%s:%d]",
		 ntype,__FILE__,__LINE__);
    }
}

int 
check_numbertype(char *numbertype)
{
    if(strcmp(numbertype,"byte")==0) return(HE5T_NATIVE_CHAR);
    else if(strcmp(numbertype,"char")==0) return(HE5T_NATIVE_CHAR);
    else if(strcmp(numbertype,"text")==0) return(HE5T_CHARSTRING);
    else if(strcmp(numbertype,"string")==0) return(HE5T_NATIVE_CHAR);  /* alias of char */
    else if(strcmp(numbertype,"sint")==0) return(HE5T_NATIVE_SHORT);
    else if(strcmp(numbertype,"int")==0) return(HE5T_NATIVE_INT);
    else if(strcmp(numbertype,"long")==0) return(HE5T_NATIVE_LONG);
    else if(strcmp(numbertype,"sfloat")==0) return(HE5T_NATIVE_FLOAT);
    else if(strcmp(numbertype,"float")==0) return(HE5T_NATIVE_DOUBLE);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_numbertype(char *numbertype)
{
  if(strcmp(numbertype,"byte")==0) return(H5T_NATIVE_CHAR);
  else if(strcmp(numbertype,"char")==0) return(H5T_NATIVE_CHAR);
  else if(strcmp(numbertype,"text")==0) return(HE5T_CHARSTRING);    /* alias of char */
  else if(strcmp(numbertype,"string")==0) return(H5T_NATIVE_CHAR);  /* alias of char */
  else if(strcmp(numbertype,"sint")==0) return(H5T_NATIVE_SHORT);
  else if(strcmp(numbertype,"int")==0) return(H5T_NATIVE_INT);
  else if(strcmp(numbertype,"long")==0) return(HE5T_NATIVE_LONG);
  else if(strcmp(numbertype,"sfloat")==0) return(H5T_NATIVE_FLOAT);
  else if(strcmp(numbertype,"float")==0) return(H5T_NATIVE_DOUBLE);
  else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_mergeflag(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_NOMERGE")==0)           return(HE5_HDFE_NOMERGE);
    else if(strcmp(numbertype,"HE5_HDFE_AUTOMERGE")==0)    return(HE5_HDFE_AUTOMERGE);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}


int 
change_entrycode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_NENTDIM")==0)           return(HE5_HDFE_NENTDIM);
    else if(strcmp(numbertype,"HE5_HDFE_NENTMAP")==0)      return(HE5_HDFE_NENTMAP);
    else if(strcmp(numbertype,"HE5_HDFE_NENTIMAP")==0)     return(HE5_HDFE_NENTIMAP);
    else if(strcmp(numbertype,"HE5_HDFE_NENTGFLD")==0)     return(HE5_HDFE_NENTGFLD);
    else if(strcmp(numbertype,"HE5_HDFE_NENTDFLD")==0)     return(HE5_HDFE_NENTDFLD);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_angleconvcode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_RAD_DEG")==0)           return(HE5_HDFE_RAD_DEG);
    else if(strcmp(numbertype,"HE5_HDFE_DEG_RAD")==0)      return(HE5_HDFE_DEG_RAD);
    else if(strcmp(numbertype,"HE5_HDFE_DMS_DEG")==0)      return(HE5_HDFE_DMS_DEG);
    else if(strcmp(numbertype,"HE5_HDFE_DEG_DMS")==0)      return(HE5_HDFE_DEG_DMS);
    else if(strcmp(numbertype,"HE5_HDFE_RAD_DMS")==0)      return(HE5_HDFE_RAD_DMS);
    else if(strcmp(numbertype,"HE5_HDFE_DMS_RAD")==0)      return(HE5_HDFE_DMS_RAD);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_subsetmode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_MIDPOINT")==0)           return(HE5_HDFE_MIDPOINT);
    else if(strcmp(numbertype,"HE5_HDFE_ENDPOINT")==0)      return(HE5_HDFE_ENDPOINT);
    else if(strcmp(numbertype,"HE5_HDFE_ANYPOINT")==0)      return(HE5_HDFE_ANYPOINT);
    else if(strcmp(numbertype,"HE5_HDFE_INTERNAL")==0)      return(HE5_HDFE_INTERNAL);
    else if(strcmp(numbertype,"HE5_HDFE_EXTERNAL")==0)      return(HE5_HDFE_EXTERNAL);
    else if(strcmp(numbertype,"HE5_HDFE_NOPREVSUB")==0)     return(HE5_HDFE_NOPREVSUB);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_gridorigincode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_GD_UL")==0)         return(HE5_HDFE_GD_UL);
    else if(strcmp(numbertype,"HE5_HDFE_GD_UR")==0)    return(HE5_HDFE_GD_UR);
    else if(strcmp(numbertype,"HE5_HDFE_GD_LL")==0)    return(HE5_HDFE_GD_LL);
    else if(strcmp(numbertype,"HE5_HDFE_GD_LR")==0)    return(HE5_HDFE_GD_LR);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_pixelregistcode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_CENTER")==0)         return(HE5_HDFE_CENTER);
    else if(strcmp(numbertype,"HE5_HDFE_CORNER")==0)    return(HE5_HDFE_CORNER);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_projcode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_GCTP_GEO")==0)            return(HE5_GCTP_GEO);
    else if(strcmp(numbertype,"HE5_GCTP_UTM")==0)       return(HE5_GCTP_UTM);
    else if(strcmp(numbertype,"HE5_GCTP_SPCS")==0)      return(HE5_GCTP_SPCS);
    else if(strcmp(numbertype,"HE5_GCTP_ALBERS")==0)    return(HE5_GCTP_ALBERS);
    else if(strcmp(numbertype,"HE5_GCTP_LAMCC")==0)     return(HE5_GCTP_LAMCC);
    else if(strcmp(numbertype,"HE5_GCTP_MERCAT")==0)    return(HE5_GCTP_MERCAT);
    else if(strcmp(numbertype,"HE5_GCTP_PS")==0)        return(HE5_GCTP_PS);
    else if(strcmp(numbertype,"HE5_GCTP_POLYC")==0)     return(HE5_GCTP_POLYC);
    else if(strcmp(numbertype,"HE5_GCTP_EQUIDC")==0)       return(HE5_GCTP_EQUIDC);
    else if(strcmp(numbertype,"HE5_GCTP_TM")==0)      return(HE5_GCTP_TM);
    else if(strcmp(numbertype,"HE5_GCTP_STEREO")==0)    return(HE5_GCTP_STEREO);
    else if(strcmp(numbertype,"HE5_GCTP_LAMAZ")==0)     return(HE5_GCTP_LAMAZ);
    else if(strcmp(numbertype,"HE5_GCTP_AZMEQD")==0)    return(HE5_GCTP_AZMEQD);
    else if(strcmp(numbertype,"HE5_GCTP_GNOMON")==0)        return(HE5_GCTP_GNOMON);
    else if(strcmp(numbertype,"HE5_GCTP_ORTHO")==0)     return(HE5_GCTP_ORTHO);
    else if(strcmp(numbertype,"HE5_GCTP_GVNSP")==0)       return(HE5_GCTP_GVNSP);
    else if(strcmp(numbertype,"HE5_GCTP_SNSOID")==0)      return(HE5_GCTP_SNSOID);
    else if(strcmp(numbertype,"HE5_GCTP_EQRECT")==0)    return(HE5_GCTP_EQRECT);
    else if(strcmp(numbertype,"HE5_GCTP_MILLER")==0)    return(HE5_GCTP_MILLER);
    else if(strcmp(numbertype,"HE5_GCTP_VGRINT")==0)        return(HE5_GCTP_VGRINT);
    else if(strcmp(numbertype,"HE5_GCTP_HOM")==0)     return(HE5_GCTP_HOM);
    else if(strcmp(numbertype,"HE5_GCTP_ROBIN")==0)       return(HE5_GCTP_ROBIN);
    else if(strcmp(numbertype,"HE5_GCTP_SOM")==0)      return(HE5_GCTP_SOM);
    else if(strcmp(numbertype,"HE5_GCTP_ALASKA")==0)    return(HE5_GCTP_ALASKA);
    else if(strcmp(numbertype,"HE5_GCTP_GOOD")==0)     return(HE5_GCTP_GOOD);
    else if(strcmp(numbertype,"HE5_GCTP_MOLL")==0)    return(HE5_GCTP_MOLL);
    else if(strcmp(numbertype,"HE5_GCTP_IMOLL")==0)     return(HE5_GCTP_IMOLL);
    else if(strcmp(numbertype,"HE5_GCTP_HAMMER")==0)    return(HE5_GCTP_HAMMER);
    else if(strcmp(numbertype,"HE5_GCTP_WAGIV")==0)        return(HE5_GCTP_WAGIV);
    else if(strcmp(numbertype,"HE5_GCTP_WAGVII")==0)     return(HE5_GCTP_WAGVII);
    else if(strcmp(numbertype,"HE5_GCTP_OBLEQA")==0)     return(HE5_GCTP_OBLEQA);
    else if(strcmp(numbertype,"HE5_GCTP_CEA")==0)    return(HE5_GCTP_CEA);
    else if(strcmp(numbertype,"HE5_GCTP_BCEA")==0)        return(HE5_GCTP_BCEA);
    else if(strcmp(numbertype,"HE5_GCTP_ISINUS")==0)     return(HE5_GCTP_ISINUS);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_tilingcode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_NOTILE")==0)         return(HE5_HDFE_NOTILE);
    else if(strcmp(numbertype,"HE5_HDFE_TILE")==0)      return(HE5_HDFE_TILE);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_compmethod(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_COMP_NONE")==0)                   return(HE5_HDFE_COMP_NONE);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_RLE")==0)               return(HE5_HDFE_COMP_RLE);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_NBIT")==0)              return(HE5_HDFE_COMP_NBIT);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SKPHUFF")==0)           return(HE5_HDFE_COMP_SKPHUFF);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_DEFLATE")==0)           return(HE5_HDFE_COMP_DEFLATE);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SZIP_CHIP")==0)         return(HE5_HDFE_COMP_SZIP_CHIP);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SZIP_K13")==0)          return(HE5_HDFE_COMP_SZIP_K13);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SZIP_EC")==0)           return(HE5_HDFE_COMP_SZIP_EC);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SZIP_NN")==0)           return(HE5_HDFE_COMP_SZIP_NN);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SZIP_K13orEC")==0)      return(HE5_HDFE_COMP_SZIP_K13orEC);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SZIP_K13orNN")==0)      return(HE5_HDFE_COMP_SZIP_K13orNN);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_DEFLATE")==0)      return(HE5_HDFE_COMP_SHUF_DEFLATE);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_SZIP_CHIP")==0)    return(HE5_HDFE_COMP_SHUF_SZIP_CHIP);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_SZIP_K13")==0)     return(HE5_HDFE_COMP_SHUF_SZIP_K13);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_SZIP_EC")==0)      return(HE5_HDFE_COMP_SHUF_SZIP_EC);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_SZIP_NN")==0)      return(HE5_HDFE_COMP_SHUF_SZIP_NN);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_SZIP_K13orEC")==0) return(HE5_HDFE_COMP_SHUF_SZIP_K13orEC);
    else if(strcmp(numbertype,"HE5_HDFE_COMP_SHUF_SZIP_K13orNN")==0) return(HE5_HDFE_COMP_SHUF_SZIP_K13orNN);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

int 
change_groupcode(char *numbertype)
{
    if(strcmp(numbertype,"HE5_HDFE_GEOGROUP")==0)              return(HE5_HDFE_GEOGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_DATAGROUP")==0)        return(HE5_HDFE_DATAGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_ATTRGROUP")==0)        return(HE5_HDFE_ATTRGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_GRPATTRGROUP")==0)     return(HE5_HDFE_GRPATTRGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_LOCATTRGROUP")==0)     return(HE5_HDFE_LOCATTRGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_PROFGROUP")==0)        return(HE5_HDFE_PROFGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_PROFGRPATTRGROUP")==0) return(HE5_HDFE_PROFGRPATTRGROUP);
    else if(strcmp(numbertype,"HE5_HDFE_GEOGRPATTRGROUP")==0)  return(HE5_HDFE_GEOGRPATTRGROUP);
    else rb_raise(rb_eHE5Error, "No such NArray type '%s' [%s:%d]",numbertype,
		  __FILE__,__LINE__);
}

void
change_chartype(hid_t numbertype, char *str)
{
    switch (numbertype){
    case 4:
    case 5:
    case 56:
    case 57: 
       strcpy(str,"char");break;
    case 13:
    case 14:
       strcpy(str,"byte");break;
    case 2:
    case 3:
    case 15:
    case 16:
       strcpy(str,"sint");break;
    case 0:
    case 1:
    case 17:
    case 18:
    case 6:
       strcpy(str,"int");break;
    case 10:
       strcpy(str,"sfloat");break;
    case 11:
       strcpy(str,"float");break;
    }
}

void
change_gridorigintype(hid_t numbertype, char *str)
{
    switch (numbertype){
    case 0: strcpy(str,"HE5_HDFE_GD_UL");break;
    case 1: strcpy(str,"HE5_HDFE_GD_UR");break;
    case 2: strcpy(str,"HE5_HDFE_GD_LL");break;
    case 3: strcpy(str,"HE5_HDFE_GD_LR");break;
    }
}

void 
change_pixelregisttype(hid_t numbertype, char *str)
{
   switch (numbertype){
    case 0: strcpy(str,"HE5_HDFE_CENTER");break;
    case 1: strcpy(str,"HE5_HDFE_CORNER");break;
    }
}


void
change_projtype(hid_t numbertype, char *str)
{
    switch (numbertype){
    case 0: strcpy(str,"HE5_GCTP_GEO");break;
    case 1: strcpy(str,"HE5_GCTP_UTM");break;
    case 2: strcpy(str,"HE5_GCTP_SPCS");break;
    case 3: strcpy(str,"HE5_GCTP_ALBERS");break;
    case 4: strcpy(str,"HE5_GCTP_LAMCC");break;
    case 5: strcpy(str,"HE5_GCTP_MERCAT");break;
    case 6: strcpy(str,"HE5_GCTP_PS");break;
    case 7: strcpy(str,"HE5_GCTP_POLYC");break;
    case 8: strcpy(str,"HE5_GCTP_EQUIDC");break;
    case 9: strcpy(str,"HE5_GCTP_TM");break;
    case 10: strcpy(str,"HE5_GCTP_STEREO");break;
    case 11: strcpy(str,"HE5_GCTP_LAMAZ");break;
    case 12: strcpy(str,"HE5_GCTP_AZMEQD");break;
    case 13: strcpy(str,"HE5_GCTP_GNOMON");break;
    case 14: strcpy(str,"HE5_GCTP_ORTHO");break;
    case 15: strcpy(str,"HE5_GCTP_GVNSP");break;
    case 16: strcpy(str,"HE5_GCTP_SNSOID");break;
    case 17: strcpy(str,"HE5_GCTP_EQRECT");break;
    case 18: strcpy(str,"HE5_GCTP_MILLER");break;
    case 19: strcpy(str,"HE5_GCTP_VGRINT");break;
    case 20: strcpy(str,"HE5_GCTP_HOM");break;
    case 21: strcpy(str,"HE5_GCTP_ROBIN");break;
    case 22: strcpy(str,"HE5_GCTP_SOM");break;
    case 23: strcpy(str,"HE5_GCTP_ALASKA");break;
    case 24: strcpy(str,"HE5_GCTP_GOOD");break;
    case 25: strcpy(str,"HE5_GCTP_MOLL");break;
    case 26: strcpy(str,"HE5_GCTP_IMOLL");break;
    case 27: strcpy(str,"HE5_GCTP_HAMMER");break;
    case 28: strcpy(str,"HE5_GCTP_WAGIV");break;
    case 29: strcpy(str,"HE5_GCTP_WAGVII");break;
    case 30: strcpy(str,"HE5_GCTP_OBLEQA");break;
    case 97: strcpy(str,"HE5_GCTP_CEA");break;
    case 98: strcpy(str,"HE5_GCTP_BCEA");break;
    case 99: strcpy(str,"HE5_GCTP_ISINUS");break;
    }
}

void 
change_tilingtype(hid_t numbertype, char *str)
{
    switch (numbertype){
    case 0: strcpy(str,"HE5_HDFE_NOTILE");break;
    case 1: strcpy(str,"HE5_HDFE_TILE");break;
    }
}

void 
change_comptype(hid_t numbertype, char *str)
{
    switch (numbertype){
    case 0: strcpy(str,"HE5_HDFE_COMP_NONE");break;
    case 1: strcpy(str,"HE5_HDFE_COMP_RLE");break;
    case 2: strcpy(str,"HE5_HDFE_COMP_NBIT");break;
    case 3: strcpy(str,"HE5_HDFE_COMP_SKPHUFF");break;
    case 4: strcpy(str,"HE5_HDFE_COMP_DEFLATE");break;
    case 5: strcpy(str,"HE5_HDFE_COMP_SZIP_CHIP");break;
    case 6: strcpy(str,"HE5_HDFE_COMP_SZIP_K13");break;
    case 7: strcpy(str,"HE5_HDFE_COMP_SZIP_EC");break;
    case 8: strcpy(str,"HE5_HDFE_COMP_SZIP_NN");break;
    case 9: strcpy(str,"HE5_HDFE_COMP_SZIP_K13orEC");break;
    case 10: strcpy(str,"HE5_HDFE_COMP_SZIP_K13orNN");break;
    case 11: strcpy(str,"HE5_HDFE_COMP_SHUF_DEFLATE");break;
    case 12: strcpy(str,"HE5_HDFE_COMP_SHUF_SZIP_CHIP");break;
    case 13: strcpy(str,"HE5_HDFE_COMP_SHUF_SZIP_K13");break;
    case 14: strcpy(str,"HE5_HDFE_COMP_SHUF_SZIP_EC");break;
    case 15: strcpy(str,"HE5_HDFE_COMP_SHUF_SZIP_NN");break;
    case 16: strcpy(str,"HE5_HDFE_COMP_SHUF_SZIP_K13orEC");break;
    case 17: strcpy(str,"HE5_HDFE_COMP_SHUF_SZIP_K13orNN");break;
    }
}
